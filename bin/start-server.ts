import config from '../config';
import App from '../src/App';
import { getLogger } from 'log4js';
import Signals = NodeJS.Signals;
import { bind as bindRoutesToApp } from '../src/routes';

const logger = getLogger('server');
const app = new App(config);
bindRoutesToApp(app);

function handleError(err: Error) {
  logger.fatal('Unhandled exception occured %s', err);
}

async function terminate(signal: Signals) {
  try {
    await app.terminate();
  } finally {
    logger.info('App is terminated with signal: %s', signal);
    process.kill(process.pid, signal);
  }
}

async function upServer() {
  const server = app.listen(config.server.port, () => {
    logger.info(`API server listening on ${ config.server.port }, in ${ config.env }`);
  });
  server.on('error', handleError);
}

// Handle uncaught errors
app.on('error', handleError);
// Start server
if (!module.parent) {
  upServer();

  const errors = ['unhandledRejection', 'uncaughtException'];
  errors.forEach((error) => {
    process.on(error as any, handleError);
  });

  const signals: Signals[] = ['SIGTERM', 'SIGINT', 'SIGUSR2'];
  signals.forEach(signal => {
    process.once(signal, () => terminate(signal));
  });
}

export default app;
