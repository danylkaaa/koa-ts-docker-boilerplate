import { loadConfig } from '@zulus/config';
import configSchema, { ConfigSchema } from './config.schema';
import * as log4js from 'log4js'

const config = loadConfig<ConfigSchema>(__dirname, configSchema, null, 'config.#env');
log4js.configure(config.log4js);

export default config;
