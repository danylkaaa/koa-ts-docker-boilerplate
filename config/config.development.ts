import { ConfigSchema } from './config.schema';
import * as path from 'path';

const config: ConfigSchema = {
  env: 'development',
  isDev: true,
  server: {
    port: 3000
  },
  log4js: {
    appenders: {
      file: {
        type: 'dateFile',
        pattern: 'yyyy-MM-dd',
        filename: path.join(process.cwd(), '/logs/server.log'),
        compress: true,
        alwaysIncludePattern: true,
        daysToKeep: 31,
        keepFileExt: true,
        numBackups: 3
      },
      console: {
        type: 'console',
        layout: {
          type: 'coloured'
        }
      },
      pureConsole: {
        type: 'stdout',
        layout: {
          type: 'dummy'
        }
      }
    },
    categories: {
      default: {
        appenders: [
          'file',
          'console'
        ],
        level: 'DEBUG'
      },
      http: {
        appenders: [
          'pureConsole'
        ],
        level: 'off'
      }
    }
  }
};
export = config;
