import * as Joi from '@hapi/joi';
import { Configuration as Log4jsConfiguration } from 'log4js';

const schema = {
  isDev: Joi.boolean()
    .default(false)
    .optional(),
  env: Joi.string().allow('development', 'production', 'test'),
  log4js: Joi.object({
    appenders: Joi.object(),
    categories: Joi.object()
  }),
  server: Joi.object({
    port: Joi.number()
  })
};

export default schema;

export interface ConfigSchema {
  env: string;
  isDev: boolean;
  log4js: Log4jsConfiguration;
  server: {
    port: number;
  };
}
