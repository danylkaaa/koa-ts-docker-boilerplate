export interface IHttpStatusObject {
  status: number,
  code: string,
  message: string
}

export interface IHttpStatusObjectContainer {
  [status: string]: IHttpStatusObject
}

const statuses: IHttpStatusObjectContainer = {
  OK: {
    status: 200,
    code: 'OK',
    message: 'Ok.'
  },
  NO_CONTENT: {
    status: 204,
    code: 'NO_CONTENT',
    message: 'No Content.'
  },
  UNKNOWN_ERROR: {
    status: 500,
    code: 'INTERNAL_SERVER_ERROR',
    message: 'The server encountered an unknown error.'
  },
  UNAUTHORIZED: {
    status: 401,
    code: 'UNAUTHORIZED',
    message: 'Authentication is needed to access the requested endpoint.'
  },
  UNKNOWN_ENDPOINT: {
    status: 404,
    code: 'NOT_FOUND',
    message: 'The requested endpoint does not exist.'
  },
  UNKNOWN_RESOURCE: {
    status: 404,
    code: 'NOT_FOUND',
    message: 'The specified resource was not found.'
  },
  INVALID_REQUEST: {
    status: 422,
    code: 'UNPROCESSABLE_ENTITY',
    message: 'The request has invalid parameters.'
  },
  INTERNAL_ERROR: {
    status: 500,
    code: 'INTERNAL_SERVER_ERROR',
    message: 'The server encountered an internal error.'
  }
};
export default statuses;
