export enum HttpMethodsEnum {
  GET,
  POST,
  PUT,
  DELETE
}

export enum AuthSchemaEnum {
  JWT,
  BASIC
}
