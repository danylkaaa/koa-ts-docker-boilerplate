import * as Koa from 'koa';
import * as bodyParser from 'koa-bodyparser';
import * as cors from 'koa-cors';
import * as compress from 'koa-compress';
import * as koaQS from 'koa-qs';
import * as helmet from 'koa-helmet';
import * as koaLogger from 'koa-logger';
import * as log4js from 'log4js';
import { Server } from 'http';
import responseHandlers from './middlewares/responseHandlers';
import errorHandler from './middlewares/errorHandler';

const httpLogger = log4js.getLogger('http');

export interface IAppConfig {

}

export default class App extends Koa {
  private _server: Server;

  constructor(config: IAppConfig) {
    super();
    // Trust proxy
    this.proxy = true;
    this._configureContext(config);
    this._configureMiddleware();
  }

  private _configureMiddleware() {
    this.use(compress());
    this.use(helmet());
    if (this.context.config.isDev) {
      this.use(koaLogger(httpLogger.debug.bind(httpLogger)));
    }
    this.use(bodyParser({
      enableTypes: ['json'],
      jsonLimit: '1mb'
    }));
    this.use(cors({
      origin: '*',
      methods: ['GET', 'HEAD', 'PUT', 'POST', 'DELETE', 'PATCH'],
      headers: ['Content-Type', 'Authorization'],
      expose: ['Content-Length', 'Date', 'X-Request-Id']
    }));
    koaQS(this, 'extended');
    this.use(responseHandlers);
    this.use(errorHandler);
  }

  private _configureContext(config: IAppConfig) {
    this.context.ROOT = process.cwd();
    this.context.config = config;
  }

  listen(...args: any[]): Server {
    this._server = super.listen(...args);
    return this._server;
  }

  async terminate() {
    this.context.knex.destroy();
    this._server.close();
  }
}
