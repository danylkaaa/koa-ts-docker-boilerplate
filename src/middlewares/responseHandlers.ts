import { HttpRequestContext } from '../index';
import statusCodes from '../constants/status-codes';
import { ParameterizedContext } from 'koa';

const clearEmptyBodyFields = (ctx: ParameterizedContext<HttpRequestContext>) => {
  if (ctx.body) {
    Object.keys(ctx.body)
      .forEach(key => {
        if (ctx.body[key] === null || ctx.body[key] === undefined) {
          delete ctx.body[key];
        }
      });
  }
};

interface IResponseHelperArguments {
  status?: number;
  data?: any;
  message?: string;
  code?: string;
}

export interface IResponseHelper {
  (options?: IResponseHelperArguments): void
}

function patch(ctx: ParameterizedContext<HttpRequestContext>) {
  ctx.responses = {};
  ctx.responses.statusCodes = statusCodes;
  ctx.responses.success = (options?: IResponseHelperArguments) => {
    const { status = null, data = null } = options || {};
    if (!!status && (status < 400)) {
      ctx.status = status;
    } else {
      ctx.status = statusCodes.OK.status;
    }
    ctx.body = {
      data
    };
  };
  ctx.responses.ok = ctx.responses.success;
  ctx.responses.error = (options?: IResponseHelperArguments) => {
    const { status = null, code = null, data = null, message = null } = options || {};
    if (!!status && (status >= 400 && status < 600)) {
      ctx.status = status;
    } else if (!(ctx.status >= 500 && ctx.status < 600)) {
      ctx.status = statusCodes.INTERNAL_SERVER_ERROR.status;
    }
    ctx.body = {
      code,
      message,
      status,
      data
    };
  };
  ctx.responses.noContent = () => {
    ctx.responses.success(statusCodes.NO_CONTENT);
  };
  ctx.responses.badRequest = (options?: IResponseHelperArguments) => {
    ctx.responses.error({
      ...statusCodes.BAD_REQUEST,
      ...options
    });
  };
  ctx.responses.unauthorized = (options?: IResponseHelperArguments) => {
    ctx.responses.error({
      ...statusCodes.UNAUTHORIZED,
      ...options
    });
  };
  ctx.responses.forbidden = (options?: IResponseHelperArguments) => {
    ctx.responses.error({
      ...statusCodes.FORBIDDEN,
      ...options
    });
  };
  ctx.responses.notFound = (options?: IResponseHelperArguments) => {
    ctx.responses.error({
      ...statusCodes.NOT_FOUND,
      ...options
    });
  };
  ctx.responses.unknownEndpoint = (options?: IResponseHelperArguments) => {
    ctx.responses.error({
      ...statusCodes.UNKNOWN_ENDPOINT,
      ...options
    });
  };
  ctx.responses.unprocessableEntity = (options?: IResponseHelperArguments) => {
    ctx.responses.error({
      ...statusCodes.UNPROCESSABLE_ENTITY,
      ...options
    });
  };
  ctx.responses.internalServerError = (options?: IResponseHelperArguments) => {
    ctx.responses.error({
      ...statusCodes.INTERNAL_SERVER_ERROR,
      ...options
    });
  };
  ctx.responses.unknownError = (options?: IResponseHelperArguments) => {
    ctx.responses.error({
      ...statusCodes.UNKNOWN_ERROR,
      ...options
    });
  };
  ctx.responses.notImplemented = (options?: IResponseHelperArguments) => {
    ctx.responses.error({
      ...statusCodes.NOT_IMPLEMENTED,
      ...options
    });
  };

}

export default async function responseHandler(ctx: ParameterizedContext<HttpRequestContext>, next: Function) {
  patch(ctx);
  await next();
  clearEmptyBodyFields(ctx);
};
