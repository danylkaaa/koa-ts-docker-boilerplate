import { HttpRequestContext } from '../index';
import { ParameterizedContext } from 'koa';

/**
 * Return middleware that handle exceptions in Koa.
 * Dispose to the first middleware.
 *
 * @return {function} Koa middleware.
 */
export default async function errorHandler(ctx: ParameterizedContext<{}, HttpRequestContext>, next: Function) {
  try {
    await next();
    // Respond 404 Not Found for unhandled request
    if (!ctx.body && (!ctx.status || ctx.status === 404)) {
      ctx.responses.unknownEndpoint();
    }
  } catch (err) {
    ctx.responses.unknownError();
    // Recommended for centralized error reporting,
    // retaining the default behaviour in Koa
    ctx.app.emit('error', err, ctx);
  }
};

