import * as passport from 'koa-passport'
import { HttpRequestContext } from '../index'
// import config from '../../config'

const { UserBasicStrategy } = require('../helpers/authStrategies');

// @ts-ignore
export default function authenticate(strategies: string[], opts: any) {
  // @ts-ignore

  return async (ctx: HttpRequestContext, next: () => any) => {
    await next();
    // await passport.authenticate(strategies,
    //   {
    //     ...opts,
    //     session: false
    //   },
    //   async (err, user) => {
    //     if (!user) {
    //       ctx.res.unauthorized({ message: err });
    //     } else {
    //       ctx.state.setAuthenticatedUser(user);
    //       await next();
    //     }
    //   }
    // )(ctx, next);
  };
}

export function initialize(opts: any) {
  passport.use('local', UserBasicStrategy);
  // passport.use('jwt-access', UserJWTStrategy.create(config.jwt.access.secret));
  // passport.use('jwt-refresh', UserJWTStrategy.create(config.jwt.refresh.secret));
  passport.initialize(opts);
}
