import * as Koa from 'koa';
import { IHttpStatusObjectContainer } from './constants/status-codes';
import { JoiObject } from '@hapi/joi';
import { Middleware } from 'koa';
import { RequestValidationSchema } from '@zulus/request-validator';
import { IResponseHelper } from './middlewares/responseHandlers';
import { AuthSchemaEnum, HttpMethodsEnum } from './constants';

export interface HttpRequestContext extends Koa.Context {
  state: object,
  responses: {
    statusCodes: IHttpStatusObjectContainer,
    ok: IResponseHelper,
    success: IResponseHelper,
    error: IResponseHelper,
    noContent: IResponseHelper,
    badRequest: IResponseHelper,
    unauthorized: IResponseHelper,
    forbidden: IResponseHelper,
    notFound: IResponseHelper,
    unknownEndpoint: IResponseHelper,
    unprocessableEntity: IResponseHelper,
    internalServerError: IResponseHelper,
    unknownError: IResponseHelper,
    notImplemented: IResponseHelper,
  },
  httpStatuses: IHttpStatusObjectContainer
}


export interface IRouteConfig {
  path: string;
  method: HttpMethodsEnum;
  children?: IRouteConfig[];
  validation?: RequestValidationSchema; // will be used first
  authentication?: AuthSchemaEnum; // will be used after validation
  middlewares?: Middleware<HttpRequestContext>[]; // will be used after authentication
  handler: Middleware<HttpRequestContext>;  // will be used last
}
