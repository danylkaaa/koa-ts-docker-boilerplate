import pingHandler from '../handlers/ping';
import { IRouteConfig } from '../index';
import { HttpMethodsEnum } from '../constants';

const routesConfigs: IRouteConfig[] = [
  {
    path: '/ping',
    method: HttpMethodsEnum.GET,
    ...pingHandler
  }
];

export default routesConfigs;
