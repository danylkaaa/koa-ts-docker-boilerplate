import App from '../App';
import * as Router from 'koa-router';
import routesConfigs from './routes-configs';
import { IRouteConfig } from '../index';
import { Middleware } from 'koa';
import {
  middleware as validationMiddleware,
  RequestValidationSchema
} from '@zulus/request-validator';
import { AuthSchemaEnum, HttpMethodsEnum } from '../constants';

const mapHttpMethod2KoaMethod: { [key: number]: string } = {
  [HttpMethodsEnum.DELETE]: 'delete',
  [HttpMethodsEnum.POST]: 'post',
  [HttpMethodsEnum.GET]: 'get',
  [HttpMethodsEnum.PUT]: 'put'
};

function appendAuthMiddlewares(routeMiddlewares: Middleware[], methods: AuthSchemaEnum[] | AuthSchemaEnum): Middleware[] {
  if (Array.isArray(methods)) {
    methods.forEach(method => appendAuthMiddlewares(routeMiddlewares, method));
  } else {
    switch (methods) {
      case AuthSchemaEnum.BASIC:
        break;
      case AuthSchemaEnum.JWT:
        break;
    }
  }
  return routeMiddlewares;
}

function appendValidationMiddlewares(routeMiddlewares: Middleware[], validationSchema: RequestValidationSchema): Middleware[] {
  routeMiddlewares.push(validationMiddleware(validationSchema));
  return routeMiddlewares;
}

function appendMiddlewares(routeMiddlewares: Middleware[], middlewares: Middleware[]) {
  middlewares.forEach(middleware => routeMiddlewares.push(middleware));
  return routeMiddlewares;
}

function bindRoute(rootRouter: Router, routeConfig: IRouteConfig) {
  const routeMiddlewares: Middleware[] = [];
  if (routeConfig.validation) {
    appendValidationMiddlewares(routeMiddlewares, routeConfig.validation);
  }
  if (routeConfig.authentication) {
    appendAuthMiddlewares(routeMiddlewares, routeConfig.authentication);
  }
  if (routeConfig.middlewares) {
    appendMiddlewares(routeMiddlewares, routeConfig.middlewares);
  }
  routeMiddlewares.push(routeConfig.handler);
  // @ts-ignore
  rootRouter[mapHttpMethod2KoaMethod[routeConfig.method]](routeConfig.path, ...routeMiddlewares);
  if (routeConfig.children) {
    bindRoutes(rootRouter, routeConfig.children);
  }
}

function bindRoutes(rootRouter: Router, routesConfig: IRouteConfig[]) {
  const subRouter = new Router();
  routesConfig.forEach(routeConfig => bindRoute(subRouter, routeConfig));
  rootRouter.use(subRouter.middleware());
  rootRouter.use(subRouter.allowedMethods());
}

export function bind(app: App) {
  const rootRouter = new Router();
  const apiRouter = new Router();
  bindRoutes(apiRouter, routesConfigs);
  rootRouter.use('/api/v1', apiRouter.middleware());
  rootRouter.use('/api/v1', apiRouter.allowedMethods());
  app.use(apiRouter.middleware());
  app.use(apiRouter.allowedMethods());
}
