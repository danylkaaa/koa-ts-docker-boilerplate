import { HttpRequestContext } from '../index';
import { ParameterizedContext } from 'koa';

async function ping(ctx: ParameterizedContext<HttpRequestContext>, next: Function) {
  ctx.responses.ok({ data: 'pong' });
  await next();
}

export default {
  handler: ping
};
