import UserBasicStrategy from './UserBasicStrategy';
import UserJWTStrategy from './UserJWTStrategy';

export default {
  UserBasicStrategy,
  UserJWTStrategy
}
