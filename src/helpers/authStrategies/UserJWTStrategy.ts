import { BasicStrategy } from 'passport-http'
import { comparePasswords } from '../passwords'
import { Request } from 'koa'

async function authenticate(req: Request, email: string, password: string, done: (...args: any[]) => any) {
  try {
    const user = await req.ctx.store.User.findByEmail(email)
      .select('email', 'password', 'uid');
    if (user && await await comparePasswords(password, user.password)) {
      done(null, await req.ctx.store.User.query().findById(user.uid));
    } else {
      done('Invalid email or password', false);
    }
  } catch (e) {
    done(null, false);
  }
}

export default new BasicStrategy({ passReqToCallback: true }, authenticate as any);
