export class BaseError extends Error {
  constructor(props: any) {
    super(props);
    this.name = this.constructor.name;
    Error.captureStackTrace(this, this.constructor);
  }
}
